--TEST--
empty last
--SKIPIF--
--FILE--
<?php
namespace WiredTiger;

include 'wt.inc';
\cleanup_wiredtiger_on_shutdown();

$wt_home = dirname(__FILE__) . '/wt-empty-last.test-db';
$wt_uri = 'table:empty-last';

$db = new Db($wt_home);
$db->create($wt_uri);
$cursor = $db->open($wt_uri);

echo "\n*** Set to last ***\n";
var_dump($cursor->last());
var_dump($cursor->key() . ' => ' . $cursor->current());

echo "\n*** Last->next will be invalid ***\n";
var_dump($cursor->next());
var_dump($cursor->valid());
var_dump($cursor->key());

?>
==DONE==
--EXPECTF--
*** Set to last ***
bool(false)
string(4) " => "

*** Last->next will be invalid ***
bool(false)
bool(false)
bool(false)
==DONE==

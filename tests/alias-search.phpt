--TEST--
alias search
--SKIPIF--
--FILE--
<?php
namespace WiredTiger;

include 'wt.inc';
\cleanup_wiredtiger_on_shutdown();

$wt_home = dirname(__FILE__) . '/wt-iterator.test-db';
$wt_uri = 'table:itetator';

$db = new Db($wt_home);
$db->create($wt_uri);
$cursor = $db->open($wt_uri);

/* Add test data, and the data will be be sorted */
$data = array(
    'First', 'Second', 'Third', 10, '', 'Last'
);

foreach ($data as $item) {
    $cursor->set($item, $item);
}

echo "\n*** Search to give key ***\n";
$cursor->search('Second');
var_dump($cursor->current());

echo "\n*** Search to a non-exist key ***\n";
$cursor->search('11');
var_dump($cursor->current());

echo "\n*** Search to a non-exist key will point to nearest next key ***\n";
$cursor->search('11', true);
var_dump($cursor->current());

?>
==DONE==
--EXPECTF--
*** Search to give key ***
string(6) "Second"

*** Search to a non-exist key ***
bool(false)

*** Search to a non-exist key will point to nearest next key ***
string(5) "First"
==DONE==
